**Welcome to personal configuration of bspwm! (deepblue)**

**Note:**

This is my first configuration and it has been made on Archlinux, hence, due to my lack of experience, I will only cover how to make possible to have this configuration on Archlinux.

![](Screenshots/2021-03-20-234946_1920x1080_scrot.png)

**NECESSARY DEPENDENCIES:**
- bspwm
- sxhkd
- ttf-material-icons-git
- otf-font-awesome
- ttf-font-awesome (need this font for polybar but you might as well change it or remove inside the polybar config file)
- ttf-fork-awesome (need this font for polybar but you might as well change it or remove inside the polybar config file)
- feh (you might as well use another program to set your background)
- polybar
- rofi (without rofi the power menu in polybar can't work)
- rofi-power-menu (check out https://github.com/jluttine/rofi-power-menu)
- speedtest-cli (check out https://github.com/ShiroUsagi-san/speedtest-polybar-module)
- pavucontrol
- pulseaudio-control (without pulseuadio the module for the volume in polybar can't work)
- picom-ibhagwan-git (you might choose to not use a compositor but you wouldn't have the same results)


**OPTIONAL DEPENDENCIES**
- xidlehook (tool for locking screen and suspend computer) https://github.com/jD91mZM2/xidlehook
- redshiftgui-bin (tool for setting monitor light)
- otf-material-icons (I advice to install this to make polybar work finest)
- papirus-icon-theme https://archlinux.org/packages/community/any/papirus-icon-theme/
- betterlockscreen (wrapper of i3lock) https://github.com/pavanjadhaw/betterlockscreen
- xorg-xrandr and libxrandr are needed if using xidlehook
- Alacritty
- Tdrop https://github.com/noctuid/tdrop
- ttf-roboto
- ttf-nerd-fonts-symbols
- Orchis-dark gtk theme https://www.gnome-look.org/p/1357889/
- Qogir cursors https://www.gnome-look.org/p/1366182/


**PROGRAMS I USE**
- xpdf (pdf reader)
- xorg-xinit (I use xinit to start bspwm session)
- scrot (take screen)
- sublime-text3 and neovim (editors)
- spacevim to customize neovim  https://spacevim.org/quick-start-guide/
- telegram-desktop-bin 
- qutebrowser and firefox (browsers)
- nnn-nerd and thunar (file managers)
- qemu, virt-manager and virt-viewer (handle virtual machines)
- tar and unzip (handle archives)
- paru (aur helper)
- lxappearance (change themes)
- nvidia (video drivers)
- bitwarden-bin (password manager)
- bpytop (htop like)
- discord
- Alacritty-themes
- qbittorrent
- teams


